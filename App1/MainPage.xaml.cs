﻿using System;
using Xamarin.Forms;

namespace App1
{
    public partial class MainPage : ContentPage
    {
        private double currentNumber;
        private string currentOperator;
        private bool isNewNumber;

        public MainPage()
        {
            InitializeComponent();
        }

        private void botonNumeros(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var number = button.Text;

            if (isNewNumber)
            {
                ResultLabel.Text = number;
                isNewNumber = false;
            }
            else
            {
                ResultLabel.Text += number;
            }
        }

        private void botonOperadores(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var newOperator = button.Text;

            if (currentOperator != null)
            {
                currentNumber = operaciones(currentNumber, double.Parse(ResultLabel.Text), currentOperator);
                ResultLabel.Text = currentNumber.ToString();
            }
            else
            {
                currentNumber = double.Parse(ResultLabel.Text);
            }

            currentOperator = newOperator;
            isNewNumber = true;
        }

        private void botonLimpiar(object sender, EventArgs e)
        {
            ResultLabel.Text = "0";
            currentNumber = 0;
            currentOperator = null;
            isNewNumber = false;
        }

        private void botonIgual(object sender, EventArgs e)
        {
            if (currentOperator != null)
            {
                var result = operaciones(currentNumber, double.Parse(ResultLabel.Text), currentOperator);
                ResultLabel.Text = result.ToString();
                currentNumber = result;
                currentOperator = null;
                isNewNumber = true;
            }
        }

        private double operaciones(double numero1, double numero2, string operador)
        {
            switch (operador)
            {
                case "+":
                    return Sumar(numero1, numero2);
                case "-":
                    return Restar(numero1, numero2);
                case "*":
                    return Multiplicar(numero1, numero2);
                case "/":
                    return Dividir(numero1, numero2);
                default:
                    throw new ArgumentException("Operacion Invalida");
            }
        }

        private double Sumar(double numero1, double numero2)
        {
            return numero1 + numero2;
        }

        private double Restar(double numero1, double numero2)
        {
            return numero1 - numero2;
        }

        private double Multiplicar(double numero1, double numero2)
        {
            return numero1 * numero2;
        }

        private double Dividir(double numero1, double numero2)
        {
            return numero1 / numero2;
        }


        private void botonPunto(object sender, EventArgs e)
        {
            if (!ResultLabel.Text.Contains("."))
            {
                ResultLabel.Text += ".";
            }
        }
    }
}
